package no.ntnu.eliaseb;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * This class tests the PostalCode class.
 */
class PostalCodeTest {

    /**
     * This nested class tests whether legal postal codes can be created.
     * This class contains only positive tests.
     */
    @Nested
    class PostalCodeWithLegalArguments{
        private boolean exception;

        /**
         * Sets the exception attribute to false before every test.
         */
        @BeforeEach
        void setUp() {
            exception = false;
        }

        /**
         * Tests whether a postal code with no leading zeroes in the zipcode can be created.
         */
        @Test
        public void zipCodeWithoutLeadingZeros() {
            try {
                PostalCode postalCode = new PostalCode("7053", "Ranheim", "Trondheim");
            }
             catch (IllegalArgumentException e) {
                exception = true;
            }
            assertFalse(exception);
        }

        /**
         * Tests whether a postal code with leading zeroes in the zipcode can be created.
         */
        @Test
        public void zipCodeWithLeadingZeros() {
            try {
                PostalCode postalCode = new PostalCode("0015", "Oslo", "Oslo");
            }
            catch (IllegalArgumentException e) {
                exception = true;
            }
            assertFalse(exception);
        }
    }

    /**
     * This nested class tests if illegal postal codes will give throw an exception.
     * This class contains only negative tests.
     */
    @Nested
    class PostalCodeWithIllegalArguments{
        private String message;

        /**
         * Sets the exception message to a blank string before every test.
         */
        @BeforeEach
        void setUp() {
            message = "";
        }

        /**
         * Tests if a postal code with a too short zipcode will raise the correct exception.
         */
        @Test
        public void tooShortZipCode() {
            try {
                PostalCode postalCode = new PostalCode("753", "Ranheim", "Trondheim");
            }
            catch (IllegalArgumentException e) {
                message = e.getMessage();
            }
            assertEquals(message, "Zip code must be a string containing exactly four digits.");
        }

        /**
         * Tests if a postal code with a too long zipcode will raise the correct exception.
         */
        @Test
        public void tooLongZipCode() {
            try {
                PostalCode postalCode = new PostalCode("70253", "Ranheim", "Trondheim");
            }
            catch (IllegalArgumentException e) {
                message = e.getMessage();
            }
            assertEquals(message, "Zip code must be a string containing exactly four digits.");
        }

        /**
         * Tests if a postal code with a zipcode containing letters will raise the correct exception.
         */
        @Test
        public void zipCodeContainsLetters() {
            try {
                PostalCode postalCode = new PostalCode("7er3", "Ranheim", "Trondheim");
            }
            catch (IllegalArgumentException e) {
                message = e.getMessage();
            }
            assertEquals(message, "Zip code must be a string containing exactly four digits.");
        }

        /**
         * Tests if a postal code with a zipcode containing spaces will raise the correct exception.
         */
        @Test
        public void zipCodeContainsSpace() {
            try {
                PostalCode postalCode = new PostalCode("72 3", "Ranheim", "Trondheim");
            }
            catch (IllegalArgumentException e) {
                message = e.getMessage();
            }
            assertEquals(message, "Zip code must be a string containing exactly four digits.");
        }

        /**
         * Tests if a postal code with a zipcode containing symbols will raise the correct exception.
         */
        @Test
        public void zipCodeContainsSymbol() {
            try {
                PostalCode postalCode = new PostalCode("72.3", "Ranheim", "Trondheim");
            }
            catch (IllegalArgumentException e) {
                message = e.getMessage();
            }
            assertEquals(message, "Zip code must be a string containing exactly four digits.");
        }

        /**
         * Tests if a postal code with an empty string zipcode will raise the correct exception.
         */
        @Test
        public void zipCodeIsEmptyString() {
            try {
                PostalCode postalCode = new PostalCode("", "Ranheim", "Trondheim");
            }
            catch (IllegalArgumentException e) {
                message = e.getMessage();
            }
            assertEquals(message, "Zip code must be a string containing exactly four digits.");
        }

        /**
         * Tests if a postal code with a null zipcode will raise the correct exception.
         */
        @Test
        public void zipCodeIsNull() {
            try {
                PostalCode postalCode = new PostalCode(null, "Ranheim", "Trondheim");
            }
            catch (IllegalArgumentException e) {
                message = e.getMessage();
            }
            assertEquals(message, "Zip code must be a string containing exactly four digits.");
        }

        /**
         * Tests if a postal code with a empty string city will raise the correct exception.
         */
        @Test
        public void cityIsEmptyString() {
            try {
                PostalCode postalCode = new PostalCode("7053", "", "Trondheim");
            }
            catch (IllegalArgumentException e) {
                message = e.getMessage();
            }
            assertEquals(message, "City can't be null or an empty string.");
        }

        /**
         * Tests if a postal code with a null city will raise the correct exception.
         */
        @Test
        public void cityIsNull() {
            try {
                PostalCode postalCode = new PostalCode("7053", null, "Trondheim");
            }
            catch (IllegalArgumentException e) {
                message = e.getMessage();
            }
            assertEquals(message, "City can't be null or an empty string.");
        }

        /**
         * Tests if a postal code with a empty string county will raise the correct exception.
         */
        @Test
        public void countyIsEmptyString() {
            try {
                PostalCode postalCode = new PostalCode("7053", "Ranheim", "");
            }
            catch (IllegalArgumentException e) {
                message = e.getMessage();
            }
            assertEquals(message, "County can't be null or an empty string.");
        }

        /**
         * Tests if a postal code with a null county will raise the correct exception.
         */
        @Test
        public void countyIsNull() {
            try {
                PostalCode postalCode = new PostalCode("7053", "Ranheim", null);
            }
            catch (IllegalArgumentException e) {
                message = e.getMessage();
            }
            assertEquals(message, "County can't be null or an empty string.");
        }
    }

    /**
     * This nested class tests that the getter methods return the correct value.
     * This class contains only positive tests.
     */
    @Nested
    class GetterTests{
        private PostalCode postalCode;

        /**
         * Sets up the postal code we will be getting from before each test.
         */
        @BeforeEach
        void setUp() {
            postalCode = new PostalCode("7053", "Ranheim", "Trondheim");
        }

        /**
         * Tests if the get ZipCode method returns the correct value.
         */
        @Test
        public void testGetZipCode(){
            assertEquals(postalCode.getZipCode(), "7053");
        }

        /**
         * Tests if the get City method returns the correct value.
         */
        @Test
        public void testGetCity(){
            assertEquals(postalCode.getCity(), "Ranheim");
        }

        /**
         * Tests if the get County method returns the correct value.
         */
        @Test
        public void testGetCounty(){
            assertEquals(postalCode.getCounty(), "Trondheim");
        }
    }

    /**
     * This nested class tests whether the setter methods sets legal values and don't set illegal values.
     * This class contains both positive and negative tests.
     */
    @Nested
    class SetterTests{
        private PostalCode postalCode;

        /**
         * Sets up the postal code that will be changed before every test.
         */
        @BeforeEach
        void setUp() {
            postalCode = new PostalCode("7053", "Ranheim", "Trondheim");
        }

        /**
         * Tests whether a legal zipcode will be set correctly.
         * This is a postitive test.
         */
        @Test
        public void testSetLegalZipCode(){
            assertTrue(postalCode.setZipCode("1234"));
            assertEquals(postalCode.getZipCode(),"1234");
        }

        /**
         * Tests whether a legal city will be set correctly.
         * This is a positive test.
         */
        @Test
        public void testSetLegalCity(){
            assertTrue(postalCode.setCity("Bergen"));
            assertEquals(postalCode.getCity(),"Bergen");
        }

        /**
         * Tests whether a legal county will be set correctly.
         * This is a positive test.
         */
        @Test
        public void testSetLegalCounty(){
            assertTrue(postalCode.setCounty("Bergen"));
            assertEquals(postalCode.getCounty(),"Bergen");
        }

        /**
         * Tests that an illegal zipcode will not be set.
         * This is a negative test.
         */
        @Test
        public void testSetIllegalZipCode(){
            assertFalse(postalCode.setZipCode(""));
            assertEquals(postalCode.getZipCode(),"7053");
        }

        /**
         * Tests that an illegal city will not be set.
         * This is a negative test.
         */
        @Test
        public void testSetIllegalCity(){
            assertFalse(postalCode.setCity(""));
            assertEquals(postalCode.getCity(),"Ranheim");
        }

        /**
         * Tests that an illegal county will not be set.
         * This is a negative test.
         */
        @Test
        public void testSetIllegalCounty(){
            assertFalse(postalCode.setCounty(""));
            assertEquals(postalCode.getCounty(),"Trondheim");
        }
    }

    /**
     * *This nested class checks whether the equals method works correctly.
     * This class contains only positive tests.
     */
    @Nested
    class EqualsTests{
        private PostalCode postalCode;
        private PostalCode postalCode2;

        /**
         * This method sets up one of the postal codes we will be using before every test.
         */
        @BeforeEach
        void setUp() {
            postalCode = new PostalCode("7053", "Ranheim", "Trondheim");
        }

        /**
         * Checks that two postal codes with identical fields returns true.
         */
        @Test
        public void everythingIdenticalEqualsTests(){
            postalCode2 = new PostalCode("7053", "Ranheim", "Trondheim");
            assertTrue(postalCode.equals(postalCode2));
        }

        /**
         * Checks that two postal codes with only zip codes identical returns true.
         */
        @Test
        public void onlyZipCodeIdenticalEqualsTests(){
            postalCode2 = new PostalCode("7053", "Bønes", "Bergen");
            assertTrue(postalCode.equals(postalCode2));
        }

        /**
         * Checks that two postal codes without identical fields returns false.
         */
        @Test
        public void nothingIdenticalEqualsTests(){
            postalCode2 = new PostalCode("1234", "Bønes", "Bergen");
            assertFalse(postalCode.equals(postalCode2));
        }

        /**
         * Checks that two postal codes with identical counties and cities but not zipcodes returns false.
         */
        @Test
        public void allButZipCodeIdenticalEqualsTests(){
            postalCode2 = new PostalCode("6053", "Ranheim", "Trondheim");
            assertFalse(postalCode.equals(postalCode2));
        }

        /**
         * Checks that comparing a postal code to a different object type returns false.
         */
        @Test
        public void differentObjectTypeEqualsTest(){
            String myString = "Test";
            assertFalse(postalCode.equals(myString));
        }

    }


}