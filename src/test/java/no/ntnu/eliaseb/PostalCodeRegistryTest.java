package no.ntnu.eliaseb;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

/**
 * This class tests the postal code registry class.
 */
class PostalCodeRegistryTest {
    PostalCodeRegistry postalCodeRegistry;

    /**
     * Sets up a postal code registry with two postal codes before every test
     */
    @BeforeEach
    public void setUp() {
        postalCodeRegistry = new PostalCodeRegistry();
        postalCodeRegistry.addPostalCode("1234", "Bønes", "Bergen");
        postalCodeRegistry.addPostalCode("6053", "Ranheim", "Trondheim");
    }

    /**
     * Checks that addPostalCode method adds a postal code successfully.
     * This is a positive test.
     */
    @Test
    public void addLegalPostalCode() {
        assertTrue(postalCodeRegistry.addPostalCode("0015", "Oslo", "Oslo"));
        assertEquals(postalCodeRegistry.getPostalCodes().size(), 3);
    }

    /**
     * Checks that addPostalCode doesn't add an illegal postal code.
     * This is an negative test.
     */
    @Test
    public void addIllegalPostalCode() {
        assertFalse(postalCodeRegistry.addPostalCode("015", "Oslo", "Oslo"));
        assertEquals(postalCodeRegistry.getPostalCodes().size(), 2);
    }

    /**
     * Checks that addPostalCode doesn't add duplicate postal codes.
     * This is an negative test.
     */
    @Test
    public void addDuplicatePostalCode() {
        assertFalse(postalCodeRegistry.addPostalCode("1234", "Oslo", "Oslo"));
        assertEquals(postalCodeRegistry.getPostalCodes().size(), 2);
    }

    /**
     * Checks that the containsPostalCode works when given a zipcode in the registry.
     * This is a positive test.
     */
    @Test
    public void containsPostalCode() {
        assertTrue(postalCodeRegistry.containsPostalCode("1234"));
    }

    /**
     * Checks that containsPostalCode method functions properly when given a zipcode not in the registry.
     * This is a positive test.
     */
    @Test
    public void doesNotContainPostalCode() {
        assertFalse(postalCodeRegistry.containsPostalCode("0015"));
    }

    /**
     * Checks that the getPostalCodes method functions properly.
     * This is an positive test.
     */
    @Test
    public void getPostalCodes() {
        ArrayList<PostalCode> postalCodes = new ArrayList<>();
        postalCodes.add(new PostalCode("1234", "Bønes", "Bergen"));
        postalCodes.add(new PostalCode("6053", "Ranheim", "Trondheim"));
        assertEquals(postalCodes, postalCodeRegistry.getPostalCodes());
    }

    /**
     * Checks that setPostalCode works when given a legal arraylist of postal codes.
     * This is a positive test.
     */
    @Test
    public void setLegalPostalCodes() {
        ArrayList<PostalCode> postalCodes = new ArrayList<>();
        postalCodes.add(new PostalCode("4321", "Kattem", "Trondheim"));
        postalCodes.add(new PostalCode("5678", "Fyllingsdalen", "Bergen"));
        assertTrue(postalCodeRegistry.setPostalCodes(postalCodes));

        assertEquals(postalCodes, postalCodeRegistry.getPostalCodes());
    }

    /**
     * Checks that the setPostalCodes doesn't set the codes when given a arraylist containing duplicate codes.
     * This is an negative test.
     */
    @Test
    public void setPostalCodesWithDuplicates() {
        ArrayList<PostalCode> postalCodes = postalCodeRegistry.getPostalCodes();

        ArrayList<PostalCode> postalCodes2 = new ArrayList<>();
        postalCodes2.add(new PostalCode("4321", "Kattem", "Trondheim"));
        postalCodes2.add(new PostalCode("5678", "Fyllingsdalen", "Bergen"));
        postalCodes2.add(new PostalCode("5678", "Stavanger", "Stavanger"));
        assertFalse(postalCodeRegistry.setPostalCodes(postalCodes2));

        assertEquals(postalCodes, postalCodeRegistry.getPostalCodes());
    }

    /**
     * Checks that the equals method returns true when comparing with an identical registry.
     * This is a positive test.
     */
    @Test
    public void testEqualsWithIdenticalRegistries() {
        PostalCodeRegistry postalCodeRegistry2 = new PostalCodeRegistry();
        postalCodeRegistry2.addPostalCode("1234", "Bønes", "Bergen");
        postalCodeRegistry2.addPostalCode("6053", "Ranheim", "Trondheim");
        assertEquals(postalCodeRegistry, postalCodeRegistry2);
    }

    /**
     * Checks that the equals method returns false when comparing with a different registry.
     * This is a positive test.
     */
    @Test
    public void testEqualsWithDifferentRegistries() {
        PostalCodeRegistry postalCodeRegistry2 = new PostalCodeRegistry();
        postalCodeRegistry2.addPostalCode("1234", "Bønes", "Bergen");
        postalCodeRegistry2.addPostalCode("5678", "Fyllingsdalen", "Bergen");
        assertNotEquals(postalCodeRegistry, postalCodeRegistry2);
    }

    /**
     * Checks that the equals method returns false when comparing with a different object type.
     * This is a positive test.
     */
    @Test
    public void testEqualsWithDifferentObjects() {
        String myString = "Test";
        assertNotEquals(postalCodeRegistry, myString);
    }

    /**
     * Checks that a postalCodeRegistry can be properly created from a text file.
     * This is a positive test.
     */
    @Test
    public void testCreatePostalCodeRegistryFromFile() {
        boolean exception = false;
        try {
            PostalCodeRegistry postalCodeRegistry2 = new PostalCodeRegistry("src/test/resources/postalCodes.txt");
            assertEquals(postalCodeRegistry, postalCodeRegistry2);
        } catch (IOException e) {
            exception = true;
        }
        assertFalse(exception);
    }

    /**
     * Checks that a postalCodeRegistry will ignore lines with
     * incorrect data or wrong formatting when reading from a file.
     * This is a positive test.
     */
    @Test
    public void testCreatePostalCodeRegistryFromFileWithoutLegalCodes() {
        boolean exception = false;
        try {
            PostalCodeRegistry postalCodeRegistry2 = new PostalCodeRegistry("src/test/resources/illegalPostalCodes.txt");
            assertEquals(postalCodeRegistry, postalCodeRegistry2);
        } catch (IOException e) {
            exception = true;
        }
        assertFalse(exception);
    }

    /**
     * Checks that an exception is thrown when file cannot be found.
     * This is an negative test.
     */
    @Test
    public void testCreatePostalCodeRegistryFromFileWithWrongPathName() {
        boolean exception = false;
        try {
            PostalCodeRegistry postalCodeRegistry2 = new PostalCodeRegistry("src/test/resources/WrongPathName.txt");
        } catch (IOException e) {
            exception = true;
        }
        assertTrue(exception);

    }

    /**
     * Checks that a postalCodes can be properly added from a text file.
     * This is a positive test.
     */
    @Test
    public void addPostalCodesFromFile() {
        PostalCodeRegistry postalCodeRegistry2 = new PostalCodeRegistry();
        assertTrue(postalCodeRegistry2.addCodesFromFile("src/test/resources/postalCodes.txt"));
        assertEquals(postalCodeRegistry, postalCodeRegistry2);
    }

    /**
     * Checks that a addCodesFromFile will ignore lines with
     * incorrect data or wrong formatting when reading from a file.
     * This is a positive test.
     */
    @Test
    public void addPostalCodesFromFileWithoutLegalCodes() {
        PostalCodeRegistry postalCodeRegistry2 = new PostalCodeRegistry();
        assertTrue(postalCodeRegistry2.addCodesFromFile("src/test/resources/illegalPostalCodes.txt"));
        assertEquals(postalCodeRegistry, postalCodeRegistry2);
    }

    /**
     * Checks that false is returned when file cannot be found.
     * This is an negative test.
     */
    @Test
    public void addPostalCodesFromFileWithWrongPathName() {
        PostalCodeRegistry postalCodeRegistry2 = new PostalCodeRegistry();
        assertFalse(postalCodeRegistry2.addCodesFromFile("src/test/resources/wrongPathName.txt"));
    }
}