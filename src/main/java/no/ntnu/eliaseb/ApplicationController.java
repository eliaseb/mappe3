package no.ntnu.eliaseb;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;


/**
 * This class represents the controller of the window.
 * It initializes starting data, and reacts to user input.
 */
public class ApplicationController {
    private FilteredList<PostalCode> filteredPostalCodes;


    @FXML
    private TextField zipCodeTextField;

    @FXML
    private TextField cityTextField;

    @FXML
    private TextField countyTextField;

    @FXML
    private Button clearFilterButton;

    @FXML
    private TableView<PostalCode> postalCodeTableView;

    @FXML
    private TableColumn<PostalCode, String> zipCodeTableColumn;

    @FXML
    private TableColumn<PostalCode, String> cityTableColumn;

    @FXML
    private TableColumn<PostalCode, String> countyTableColumn;

    /**
     * Creates an application controller instance.
     * @param postalCodes The postal codes that are used to create the postal code filtered list.
     */
    public ApplicationController(ArrayList<PostalCode> postalCodes) {
        //Gets the postal codes and puts them in an Observable list.
        ObservableList<PostalCode> observablePostalCodes = FXCollections.observableArrayList(postalCodes);
        //Puts the observable list in a filtered list.
        this.filteredPostalCodes = new FilteredList(observablePostalCodes);
    }

    /**
     * Is called after the @FXML fields are populated.
     * Initializes the starting data for the tableView.
     * Sets the cell value factorise for the table columns.
     */
    @FXML
    void initialize() {
        //Sets the tableView data.
        postalCodeTableView.setItems(filteredPostalCodes);
        //Sets the tableColumn cellValueFactories.
        zipCodeTableColumn.setCellValueFactory(celldata -> new SimpleStringProperty(celldata.getValue().getZipCode()));
        cityTableColumn.setCellValueFactory(celldata -> new SimpleStringProperty(celldata.getValue().getCity()));
        countyTableColumn.setCellValueFactory(celldata -> new SimpleStringProperty(celldata.getValue().getCounty()));

    }

    /**
     * Updates the predicate of the filtered list of postal codes.
     * The predicate checks if the postal code's fields contain
     * the string from their corresponding text field.
     * Case sensitivity is ignored when checking city and county.
     */
    public void updateFilter(){
        filteredPostalCodes.setPredicate(postalCode -> postalCode.getZipCode().contains(zipCodeTextField.getText()) &&
                StringUtils.containsIgnoreCase(postalCode.getCity(), cityTextField.getText()) &&
                StringUtils.containsIgnoreCase(postalCode.getCounty(), countyTextField.getText()));
    }

    /**
     * Updates the predicate of the filtered list.
     * This method is triggered when user types in the county text field.
     * @param event The key event that triggered this method.
     */
    @FXML
    void onCountyKeyPressed(KeyEvent event) {
        updateFilter();
    }

    /**
     * Updates the predicate of the filtered list.
     * This method is triggered when user types in the city text field.
     * @param event The key event that triggered this method.
     */
    @FXML
    void onCityKeyPressed(KeyEvent event) {
        updateFilter();
    }

    /**
     * Removes all characters that are not digits from the zip code text field.
     * Updates the predicate of the filtered list.
     * This method is triggered when user types in the zip code text field.
     * @param event The key event that triggered this method.
     */
    @FXML
    void onZipCodeKeyPressed(KeyEvent event) {
        //Removes all non-digit characters from the list.
        zipCodeTextField.setText(zipCodeTextField.getText().replaceAll( "[^\\d]", "" ));
        //The previous line  moves the caret to the back of the text field, so this will move it to the front again.
        zipCodeTextField.positionCaret(zipCodeTextField.getText().length());
        //Updates the predicate.
        updateFilter();
    }

    /**
     * Resets the predicate of the filtered list, and clears the text fields.
     * This method is triggered when the user clicks the clear filters button.
     * @param event the event that triggered this event.
     */
    @FXML
    void onClearFilterButton(ActionEvent event) {
        //Removes the text from the filter text fields.
        zipCodeTextField.clear();
        cityTextField.clear();
        countyTextField.clear();
        //Resets the filteredPostalCodes predicate.
        updateFilter();
    }


}
