package no.ntnu.eliaseb;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * This class represents a registry of postal codes.
 * It has one field consisting of an arraylist of postal codes.
 * The registry can read from files, and ensures that there are no duplicate postal codes.
 */
public class PostalCodeRegistry {
    private ArrayList<PostalCode> postalCodes;

    /**
     * Creates a new, empty postal code.
     */
    public PostalCodeRegistry() {
        this.postalCodes = new ArrayList<>();
    }

    /**
     * Creates a new postal code, and fills it with info from a file.
     * @param pathName The path to the file it reads from.
     * @throws IOException If file can't be found or read.
     */
    public PostalCodeRegistry (String pathName) throws IOException {
        //Creates the arraylist of names.
        postalCodes= new ArrayList<>();
        //Adds the codes from the files.
        if(!addCodesFromFile(pathName)){
            throw new IOException("Something went wrong when reading the file.");
        }
    }

    /**
     * Adds the registry with postal codes form a file.
     * @param pathName The path to the file it reads from.
     * @return true if all the lines where read, false if an IOException occurred.
     */
    public boolean addCodesFromFile(String pathName){
        //Tries to read the postal codes file.
        try(BufferedReader bufferedReader = new BufferedReader(new FileReader(pathName))){
            String line;
            //Reads the data from the text file, one line at the time.
            while ((line=bufferedReader.readLine()) != null){
                try {

                    //Gets the data about the postal code from the line.
                    String zipCode = line.split("\t")[0];
                    String city = line.split("\t")[1];
                    String county = line.split("\t")[3];
                    //Adds the postal code to the registry.
                    addPostalCode(zipCode, city, county);
                } catch (Exception e){
                    //Skips lines that can't be read or that hold illegal data.
                }
            }
            //All the lines where read or skipped.
            return true;
        } catch (IOException e) {
            //IOException occurred
            //e.printStackTrace();
            return false;
        }
    }

    /**
     * Creates and adds a postal code if it is legal and not already in the registry.
     * @param zipCode The zipCode of the new postal code.
     * @param city The city of the new postal code.
     * @param county The county of the new postal code.
     * @return True if the postal code was added successfully, false if not.
     */
    public boolean addPostalCode(String zipCode, String city, String county){
        //Checks if the registry contains a postal code with the same zipCode.
        if(!containsPostalCode(zipCode)){
            //If not, we try to create and add the postal code.
            try{
                return postalCodes.add(new PostalCode(zipCode, city, county));
            } catch (IllegalArgumentException e){
                //e.printStackTrace();
            }
        }
        //If the registry contains the zipCode, or the postal code was not legal, we return false.
        return false;
    }


    /**
     * Checks if the arraylist contains any postal codes with the same zipCode.
     * @param zipCode The zipCode we are checking if is contained in the arraylist.
     * @return True if the arraylist contains a postal code with the same code, false if not.
     */
    public boolean containsPostalCode(String zipCode){
        return postalCodes.stream().anyMatch(postalCode -> postalCode.getZipCode().equals(zipCode));
    }

    /**
     * Gets the arraylist of postal codes.
     * @return The arraylist of postal codes.
     */
    public ArrayList<PostalCode> getPostalCodes() {
        return postalCodes;
    }

    /**
     * Sets the arraylist of postal codes.
     * @param postalCodes The arraylist of new postal codes.
     * @return true if the arraylist was set, false if it was not.
     */
    public boolean setPostalCodes(ArrayList<PostalCode> postalCodes) {
        //Checks if the arraylist contains duplicates.
        if (postalCodes.size() == new HashSet<>(postalCodes).size()){
            this.postalCodes = postalCodes;
            return true;
        }
        return false;
    }

    /**
     * Checks if the registry is equal to another object.
     * @param o The object we are checking against.
     * @return True if they are equal, false if they are not.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PostalCodeRegistry that = (PostalCodeRegistry) o;
        return Objects.equals(postalCodes, that.postalCodes);
    }

    /**
     * Generates the hashcode for the registry.
     * @return the hashcode for this postal code registry.
     */
    @Override
    public int hashCode() {
        return Objects.hash(postalCodes);
    }

    /**
     * Creates a string consisting of the values of all of the fields in the registry.
     * @return The string consisting of the values of all of the fields in the registry.
     */
    @Override
    public String toString() {
        return "PostalCodeRegistry{" +
                "postalCodes=" + postalCodes +
                '}';
    }
}
