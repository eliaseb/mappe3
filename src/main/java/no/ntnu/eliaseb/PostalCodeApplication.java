package no.ntnu.eliaseb;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * This class is the application and inherits from the abstract class Application.
 * This class creates the main window.
 * It sets up the registry and controller and finally shows the window.
 */
public class PostalCodeApplication extends Application {

    /**
     * The start method starts the application and sets up the registry and controller.
     * @param primaryStage The stage where the scene is shown.
     * @throws IOException If the fxml loader cant read from the fxml file.
     */
    @Override
    public void start(Stage primaryStage) throws IOException {
        //Loads the postal codes.
        PostalCodeRegistry postalCodeRegistry = new PostalCodeRegistry("src/main/resources/postalCodes.txt");
        //Creates the controller for the application.
        ApplicationController controller = new ApplicationController(postalCodeRegistry.getPostalCodes());
        //Sets up the application window.
        FXMLLoader loader = new FXMLLoader(controller.getClass().getResource("/main.fxml"));
        loader.setController(controller);
        primaryStage.setScene(new Scene(loader.load()));
        //Shows the application window.
        primaryStage.show();
    }
}
