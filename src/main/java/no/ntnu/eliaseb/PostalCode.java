package no.ntnu.eliaseb;

import java.util.Objects;

/**
 * This class represents a postal code and stores information about it.
 * It has fields for the zip code, the corresponding city, and the county it is in.
 * Postal codes with the same zip code are considered equal.
 */
public class PostalCode {
    private String zipCode;
    private String city;
    private String county;

    /**
     * Creates a new instance of postal code.
     * @param zipCode The zip code of the postal.
     * @param city The corresponding city of the postal code.
     * @param county The county of the postal code.
     * @throws IllegalArgumentException If illegal arguments are passed.
     */
    public PostalCode(String zipCode, String city, String county) throws IllegalArgumentException{
        //Checks if the arguments are legal, throws an exception if not.
        checkForIllegalArgument(zipCode, city, county);
        //Creates the postal code.
        this.zipCode = zipCode;
        this.city = city;
        this.county = county;

    }


    /**
     * Checks if arguments are legal, throws an exception if not.
     * @param zipCode The zip code we are checking if is legal.
     * @param city the city name we are checking if is legal.
     * @param county The county we are checking if is legal.
     * @throws IllegalArgumentException if is any of the arguments are illegal.
     */
    public void checkForIllegalArgument(String zipCode, String city, String county) throws IllegalArgumentException{
        //Checks that the zip code is not null, has length four, an consists solely of digits.
        if (illegalZipCode(zipCode)){
            throw new IllegalArgumentException("Zip code must be a string containing exactly four digits.");
        }
        //Checks that the city is not null or an empty string.
        if (illegalCity(city)) {
            throw new IllegalArgumentException("City can't be null or an empty string.");
        }
        //Checks that the county is not null or an empty string.
        if (illegalCounty(county)){
            throw new IllegalArgumentException("County can't be null or an empty string.");
        }
    }

    /**
     * Checks if the zipcode is legal.
     * The zip code must be a string four characters long and must consist exclusively of digits.
     * @param zipCode The zipcode we are checking
     * @return true if the zipcode was legal, false if it was not.
     */
    private boolean illegalZipCode(String zipCode){
        return zipCode == null || zipCode.length() != 4 || !zipCode.matches("^[0-9]+");
    }

    /**
     * Checks if the city is legal.
     * The city can't be null or an empty string.
     * @param city The city we are checking
     * @return true if the city was legal, false if it was not.
     */
    private boolean illegalCity(String city){
        return city == null || city.equals("");
    }

    /**
     * Checks if the county is legal.
     * The county can't be null or an empty string.
     * @param county The county we are checking
     * @return true if the county was legal, false if it was not.
     */
    private boolean illegalCounty(String county){
        return county == null || county.equals("");
    }

    /**
     * Gets the zip code of the postal code.
     * @return the zip code of the postal code
     */
    public String getZipCode() {
        return zipCode;
    }

    /**
     * Sets the zip code of the postal code if the zip code is legal.
     * @param zipCode the new zip code of the postal code.
     * @return true if the zipcode was set, false if it was not.
     */
    public boolean setZipCode(String zipCode) {
        if(!illegalZipCode(zipCode)){
            this.zipCode = zipCode;
            return true;
        }
        return false;
    }

    /**
     * Gets the city of the postal code.
     * @return The city of the postal code.
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets the city of the postal code if the city is legal.
     * @param city The new city of the postal code.
     * @return true if the city was set, false if it was not.
     */
    public boolean setCity(String city) {
        if(!illegalCity(city)){
            this.city = city;
            return true;
        }
        return false;
    }

    /**
     * Gets the county of the postal code.
     * @return The county of the postal code.
     */
    public String getCounty() {
        return county;
    }

    /**
     * Sets the county of the postal code if the county is legal.
     * @param county The new county of the postal code.
     * @return true if the county was set, false if it was not.
     */
    public boolean setCounty(String county) {
        if(!illegalCounty(county)){
            this.county = county;
            return true;
        }
        return false;
    }

    /**
     * Checks if the postal code is equal to another object.
     * Two postal codes are equal if they have the same zip code.
     * @param o The object we are checking against.
     * @return True if they are equal, false if they are not.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PostalCode that = (PostalCode) o;
        return zipCode.equals(that.zipCode);
    }

    /**
     * Generates the hashcode for the postal code.
     * @return The hashcode for the postal code.
     */
    @Override
    public int hashCode() {
        return Objects.hash(zipCode);
    }

    /**
     * Generates a string with all the values of the postal code.
     * @return The string of  the values of all the fields.
     */
    @Override
    public String toString() {
        return "PostalCode{" +
                "code='" + zipCode + '\'' +
                ", postOffice='" + city + '\'' +
                ", county='" + county + '\'' +
                '}';
    }
}
