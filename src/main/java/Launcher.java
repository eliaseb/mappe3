import javafx.application.Application;
import no.ntnu.eliaseb.PostalCodeApplication;

/**
 * This class serves as a launcher for the application.
 */
public class Launcher {
    /**
     * The main method of the launcher.
     * Launches the Postal code application.
     * @param args The command line arguments.
     */
    public static void main(String[] args) {
        System.out.println("Launching Application...");
        Application.launch(PostalCodeApplication.class, args);
    }
}
